import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCIg8jISb3A-9e3doZ-hTWpPKL8s4lUfp4',
    libraries: 'places'
  }
})
